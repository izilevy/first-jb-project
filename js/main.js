/* 

PROJECT 1 - March 2020
Student name: Izidor Levy
Course Details: Fullstack Web Development 18

The tasks system is using the LocalStorage option to save the data to the user browser local storage.
Everytime the user will load the page, the data will be reloaded from it's local storage.

The tasks data will be loaded and saved in the array called: "tasks".

The program will build all the tasks from JS DOM manipulation and use Bootstrap design.

***/


// Main variable for saving the tasks list data
var tasks = [];

// Start the page build and clears any FADE IN effect from the newest task so the effect won't be showed when refreshing the page
function pageLoad() {
  // Check if there is any saved data in the LocalStorage and load it
  checkDB();
  // Runnging the main functions
  taskMaker();
  // Clear FADE IN effect
  if (tasks.length > 0) {
    document.getElementById("task-"+((tasks.length)-1)).classList.remove("fade-in");
  }
}

// Main function which checks if there is any data in the LocalStorage and loads it if there is any
function checkDB() {
  var localstorageTasksData = localStorage.getItem('myTasks');
  if (localstorageTasksData != null) {
    tasks = JSON.parse(localstorageTasksData);
  }
}

// Function which clears the fields of the entered arguments
function clearField() {
  for (let i = 0; i < arguments.length; i++) {
    document.getElementById(arguments[i]).value = '';
    document.getElementById(arguments[i]).style.textAlign = 'left';
    document.getElementById(arguments[i]).style.direction = 'ltr'; 
  }
}

// Function which clears all the tasks in the array and LocalStorage
function clearTasks() {
  var answer = confirm("Are you sure you want to DELETE ALL of the task?");
    if (answer == false) {
      return;
    } else {
      tasks = [];
      localStorage.setItem('myTasks', JSON.stringify(tasks));
      taskMaker();
    }
}

// Main function which updates and loads the data to the web window
function taskMaker() {
  let tasksList = document.getElementById("tasks-list");
  tasksList.innerText = "";
  // Callsing the tasks from last to first in the array in order to add the tasks to the windows from left to right - newest ones on the upper left side
  for (let i = tasks.length-1; i >= 0; i--) {
    let card = document.createElement("div");
    card.classList.add("card", "m-1", "border-0");
    card.id = "task-" + i;
    // Checks if the tasks is a new task to add it a FADE IN effect
    if (tasks[i].new) {
      card.classList.add("fade-in");
      tasks[i].new = false;
    }
    let cardBackground = document.createElement("img");
    cardBackground.classList.add("card-img-top");
    cardBackground.src = "img/notebg.png";
    cardBackground.alt = "Task paper image";
    let cardInside = document.createElement("div");
    cardInside.classList.add("card-img-overlay", "pl-1", "pt-3");
    let deleteTask = document.createElement("div");
    deleteTask.classList.add("float-right", "delete", "mr-1", "mb-1");
    deleteTask.innerHTML = '<i class="fas fa-times-circle"></i>';
    deleteTask.setAttribute("onclick", "deleteTask(" + i + ")");
    let cardText = document.createElement("div");
    cardText.classList.add("card-text", "overflow-auto", "m-1", "float-left");
    cardText.style.width = "11.56rem";
    cardText.style.height = "10.3rem";
    // Checks if the input text is RTL type of text to adjust the direction of the task text
    if (/[\u0590-\u06FF]/.test(tasks[i].task)) {
      cardText.style.textAlign = "right";
      cardText.style.direction = "rtl";
    }
    cardText.innerText = tasks[i].task;
    let cardDate = document.createElement("div");
    cardDate.classList.add("card-text", "float-left", "mx-1", "my-1", "small");
    cardDate.style.width = "11.56rem";
    cardDate.innerText = tasks[i].date;
    let cardTime = document.createElement("div");
    cardTime.classList.add("card-text", "float-left", "mx-1", "small");
    cardTime.innerText = tasks[i].time;

    card.appendChild(cardBackground);
    cardInside.appendChild(deleteTask);
    cardInside.appendChild(cardText);
    cardInside.appendChild(cardDate);
    cardInside.appendChild(cardTime);
    card.appendChild(cardInside);

    tasksList.appendChild(card);
  }
}

// Sets the new task textarea typing direction according to the input language
function autoDirection() {
  let textArea = document.getElementById("n-task");
  if (/[\u0590-\u06FF]/.test(textArea.value)) {
    textArea.style.direction = "rtl";
    textArea.style.textAlign = "right";
  } else {
    textArea.style.direction = "ltr";
    textArea.style.textAlign = "left";
  }
}

// Adding a task to the tasks data array and LocalStorage
function addTask() {
    let nTask = document.getElementById("n-task").value;
    let nDate = document.getElementById("n-date").value;
    let nTime = document.getElementById("n-time").value;
    let nDateR = nDate.split("-").reverse().join("-"); // Reversing the Date format for Israel localization
    if (nTask != '' && nDate != '') {
      newTask = {
        task: nTask,
        date: nDateR,
        time: nTime,
        new: true
      }
      tasks.push(newTask);
      localStorage.setItem('myTasks', JSON.stringify(tasks));
      clearField("n-task", "n-date", "n-time");
      taskMaker();
    }
    return false;
}

// Deleting a task from the tasks data array and LocalStorage
function deleteTask(i) {
  tasks.splice(i, 1);
  localStorage.setItem('myTasks', JSON.stringify(tasks));
  taskMaker();
}